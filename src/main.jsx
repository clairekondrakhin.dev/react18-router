import React from "react";
import ReactDOM from "react-dom/client";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Cart from "./pages/cart/Cart";
import Home from "./pages/home/Home";
import ProductList from "./pages/products/ProductList";
import "./index.css";
import ErrorPage from "./components/ErrorPage";


// 
const router = createBrowserRouter([
  {
    path: "/", // Chemin de la route
    element: <Home />, // Composant à afficher lorsque la route est atteinte
    errorElement: <ErrorPage />, // Composant à afficher en cas d'erreur
  },
  {
    path: "/ProductList",
    element: <ProductList />,
    errorElement: <ErrorPage />,
  },
  {
    path: "/Cart",
    element: <Cart />,
    errorElement: <ErrorPage />,
  },
]);

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>
);
