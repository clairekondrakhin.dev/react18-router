import { NavLink } from "react-router-dom";
import "./Navigation.css";


// création d'un composant navigation. celui-ci sera importé dans la page Home
// Les NavLink sont utilisés chacun avec une prop 'to' spécifiant le chemin de destination.
function Navigation() {
  return (
    <nav>
      <div className="Navigation">
        <NavLink to="/">
          <p>Home</p>
        </NavLink>

        <NavLink to="/productList">
          <p>Products</p>
        </NavLink>

        <NavLink to="/Cart">
          <p>Cart</p>
        </NavLink>
      </div>
    </nav>
  );
}

export default Navigation;
